'use strict';

// This class is used for logins
class Login {
  constructor(hash) {
    this.sessions = [];
    this.users = [];
    this.passwords = [];
    Object.keys(hash).map(user => {
      this.registerUser(user, hash[user]);
    });
  }

  logout(user) {
    this.sessions = this.sessions.filter(session => session !== user);
  }

  // Checks if user exists
  userExists(user) {
    return this.users.indexOf(user) >= 0;
  }

  // Register user
  registerUser(user, password) {
    //check if the user exists
    if(this.userExists(user)){
      console.error(`User ${user} already exists.`)
      return false;
    }
    this.users.push(user);
    this.passwords.push(password);
  }

  removeUser(user) {
    let index = this.idx(user, this.users);
    this.users[index] = null;
    this.passwords[index] = null;
    this.users = this.users.filter(user => user !== null);
    this.passwords = this.passwords.filter(password => password !== null);
  }

  checkPassword(user, password) {
    let index = this.users.indexOf(user);
    return this.passwords[index] === password;
  }

  updatePassword(user, oldPassword, newPassword) {
    // First we check if the user exists
    if(this.userExists(user)){
      //check credentials
      if(this.checkPassword(user, oldPassword)){
        //get user index
        let index = this.users.indexOf(user);
        this.passwords[index] = newPassword
      }  
    }
    return false;
  }

  login(user, password) {
    //check if the user exists
    if(this.userExists(user)){
      // check if creadentials match
      if(this.checkPassword(user, password)){
        this.sessions.push(user);
      }
    }
  }

  // Gets index of an element in an array
  idx(element, array) {
    let cont=0;
    for (let i of array) {
      if (i === element) {
        return cont;
      }
      cont += 1;
    }
    return cont;
  }
}

module.exports = Login;

/*
let registeredUsers = {
  user1: 'pass1',
  user2: 'pass2',
  user3: 'pass3'
};

let login = new Login(registeredUsers);

login.registerUser('user4', 'pass4');
login.login('user4', 'pass4');
login.updatePassword('user3', 'pass3', 'pass5');
login.login('user3', 'pass5');
login.logout('user4');
login.logout('user3');

*/