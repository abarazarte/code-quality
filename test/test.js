var expect = require('chai').expect;
var Login = require('..');

describe('Code refactor to Login class', function(){
    var login;

    before(function(){
        let registeredUsers = {
            user1: 'pass1',
            user2: 'pass2',
            user3: 'pass3'
        };
        login = new Login(registeredUsers);
    });

    it('Should be instance of Login', function(){
        expect(login).to.be.an.instanceof(Login);
    });

    it('Initial registeredUsers length should be 3', function(){
        expect(login.users.length).to.be.equal(3);
    });

    it('Should register user4', function(){
        login.registerUser('user4', 'pass4');
        expect(login.userExists('user4')).to.be.true;
    });

    it('Should login user4', function(){
        login.login('user4', 'pass4');
        expect(login.sessions).to.include('user4');
        expect(login.sessions.length).to.be.equal(1);
    });

    it('Should update user3\'s password', function(){
        login.updatePassword('user3', 'pass3', 'pass5');
        expect(login.checkPassword('user3', 'pass5')).to.be.true;
    });

    it('Should login user3 with the new password', function(){
        login.login('user3', 'pass5');
        expect(login.sessions).to.include('user3');
        expect(login.sessions.length).to.be.equal(2);
    });

    it('Should logout user4', function(){
        login.logout('user4');
        expect(login.sessions).to.not.include('user4');
        expect(login.sessions.length).to.be.equal(1);
    });
    
    it('Should logout user3', function(){
        login.logout('user3');
        expect(login.sessions).to.not.include('user3');
        expect(login.sessions.length).to.be.equal(0);
    });
});